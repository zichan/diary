from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, FormView, UpdateView, DetailView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from Entry.models import Entry
from Entry.forms import createEntry
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from guardian.shortcuts import assign_perm
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from Entry.serializers import EntrySerializer, EntryViewSerializer
from rest_framework import mixins, generics, status
from rest_framework.response import Response
from Entry.permissions import CanView

class JSONResponse(HttpResponse):
        def __init__(self, data, **kwargs):
            content = JSONRenderer().render(data)
            kwargs['content_type'] = 'application/json'
            super(JSONResponse, self).__init__(content, **kwargs)


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)

@permission_classes((IsAuthenticated,CanView))
@api_view(['GET','POST'])
def entry_list(request):
    if request.method == 'GET':
        snippets = Entry.objects.filter(user = request.user)
        serializer = EntrySerializer(snippets, many=True)
        return Response(serializer.data)
    if request.method == 'POST':
        serializer = EntryViewSerializer

@permission_classes((IsAuthenticated,CanView))
@api_view(['GET','PUT'])
def entry_view(request, pk):
    try:
        entry = Entry.objects.get(pk=pk)
    except Entry.DoesNotExist:
        return  Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = EntryViewSerializer(entry)
        return Response(serializer.data)

    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = EntrySerializer(entry, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer._errors, status=400)

class EntryList(generics.ListAPIView):
    serializer_class = EntrySerializer
    def get_queryset(self):
        return Entry.objects.filter(user = self.request.user)

class home(LoginRequiredMixin, ListView):
    template_name='entry_list.html'
    model = Entry
    paginate_by = 10
    def get_queryset(self):
       return Entry.objects.filter(user = self.request.user).order_by('-dateEnd', '-dateBegin')

class create(LoginRequiredMixin, FormView):
    form_class = createEntry
    template_name = 'entry_create.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.save()
        assign_perm('update_Entry', self.request.user, form.instance)
        assign_perm('view_Entry', self.request.user, form.instance)
        return super(create, self).form_valid(form)

    def get_success_url(self):
        return reverse("home")


class update(LoginRequiredMixin, UpdateView):
    form_class = createEntry
    template_name = 'entry_update.html'
    def form_valid(self, form):
        form.save()
        return super(update, self).form_valid(form)

    def get_success_url(self):
        return reverse('home')

    def get(self, request, **kwargs):
        self.object = Entry.objects.get(id=self.kwargs['pk'])
        if not self.request.user.has_perm('update_Entry', self.object):
            raise PermissionDenied()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def get_object(self, queryset=None):
        obj = Entry.objects.get(id=self.kwargs['pk'])
        if not obj.user == self.request.user:
            raise Http404
        return obj


class EntryDetailView(LoginRequiredMixin, DetailView):
    model = Entry
    template_name = "entry_view.html"
    context_object_name = 'Entry'

    def get(self, request, *args, **kwargs):
        try:
            self.object = Entry.objects.get(id=self.kwargs['pk'])
        except Entry.DoesNotExist:
            raise Http404
        if not self.request.user.has_perm('view_Entry', self.object):
            raise PermissionDenied()
        return super(EntryDetailView, self).get(request, *args, **kwargs)


ckeditor_form_view = create.as_view()
ckeditor_update_view = update.as_view()