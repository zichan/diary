from _ast import mod
from django.db import models
import datetime
from ckeditor.widgets import CKEditorWidget
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.contrib import admin
# Create your models here.


class Entry(models.Model):
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    text = RichTextField(config_name='default')
    title = models.TextField()
    dateBegin = models.DateField()
    dateEnd = models.DateField()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        """
            Override save so as to maintain modified and created date
        """
        if not self.id:
            self.created = datetime.datetime.today()
        self.modified = datetime.datetime.today()
        return super(Entry, self).save(*args, **kwargs)

    class Meta:
        permissions = (
            ('view_Entry', 'View Entry'),
            ('update_Entry', 'Update Entry'),
        )