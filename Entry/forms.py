__author__ = 'jacob.cummings'
from django import forms
from Entry.models import Entry
from ckeditor.fields import RichTextFormField


class createEntry(forms.ModelForm):
    title = forms.CharField()
    text = RichTextFormField(config_name='default')
    dateBegin = forms.DateField()
    dateEnd = forms.DateField()
    def __init__(self, *args, **kwargs):
        super(createEntry, self).__init__(*args, **kwargs)
        self.fields["dateBegin"].label = "Start date of entry"
        self.fields["dateEnd"].label = "End date of entry"
    class Meta:
        model = Entry
        fields = ['title', 'text', 'dateBegin', 'dateEnd']