from rest_framework import serializers
from Entry.models import Entry

class EntrySerializer(serializers.HyperlinkedModelSerializer):
    view = serializers.HyperlinkedIdentityField(view_name='detail')
    editview = serializers.HyperlinkedIdentityField(view_name='update')

    def restore_object(self, attrs, instance=None):
        if instance:
            instance.title = attrs.get('title', instance.title)
            instance.text = attrs.get('text', instance.text)
            instance.endDate = attrs.get('endDate', instance.endDate)
            instance.startDate = attrs.get('startDate', instance.startDate)
            return instance
        return Entry(**attrs)

    class Meta:
        model = Entry
        fields = ('id', 'title', 'text', 'view', 'editview')

class EntryViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ('id', 'title', 'text', 'dateBegin', 'dateEnd', 'created', 'modified')