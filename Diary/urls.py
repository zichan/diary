from django.conf.urls import patterns, include, url

import Entry.views
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
admin.autodiscover()
from django.views.generic import TemplateView
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Diary.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^$', 'Entry.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^$', Entry.views.home.as_view(), name='home'),
    url(r'^new/$', Entry.views.create.as_view(), name='new'),
    url(r'^update/(?P<pk>\d+)/$', Entry.views.update.as_view(), name='update'),
    url(r'^entry/(?P<pk>\d+)/$', Entry.views.EntryDetailView.as_view(), name='detail'),
    url(r'^logout/$', 'django.contrib.auth.views.logout',{'next_page': 'home'}, name='logout'),
    (r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^API/entrys/$','Entry.views.entry_list', name='listJSON'),
    url(r'^API/entrys/(?P<pk>[0-9]+)$', 'Entry.views.entry_view', name='viewJSON'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
